EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "ADC node"
Date "2020-05-05"
Rev ""
Comp "i_a"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ADC_node:ISO7741DBQR U3
U 1 1 5E9DCBC7
P 6800 4250
F 0 "U3" H 6800 4965 50  0000 C CNN
F 1 "ISO7741DBQR" H 6800 4874 50  0000 C CNN
F 2 "Package_SO:SSOP-16_3.9x4.9mm_P0.635mm" H 6300 4650 50  0001 C CNN
F 3 "" H 6300 4650 50  0001 C CNN
	1    6800 4250
	1    0    0    -1  
$EndComp
$Comp
L ADC_node:ADS8681IPWR U7
U 1 1 5E9DF40F
P 12400 4300
F 0 "U7" H 12400 5065 50  0000 C CNN
F 1 "ADS8681IPWR" H 12400 4974 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 12250 5000 50  0001 C CNN
F 3 "" H 12250 5000 50  0001 C CNN
	1    12400 4300
	-1   0    0    -1  
$EndComp
$Comp
L ADC_node:LP2981AIM5-5.0 U6
U 1 1 5E9E0F5B
P 9500 1900
F 0 "U6" V 8935 1875 50  0000 C CNN
F 1 "LP2981AIM5-5.0" V 9026 1875 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 9100 2300 50  0001 C CNN
F 3 "" H 9100 2300 50  0001 C CNN
	1    9500 1900
	0    1    1    0   
$EndComp
$Comp
L ADC_node:ESP32-LOLIN U1
U 1 1 5E9E721A
P 6850 8500
F 0 "U1" H 6825 9825 50  0000 C CNN
F 1 "ESP32-LOLIN" H 6825 9734 50  0000 C CNN
F 2 "ADC_node:esp32-lolin" H 6100 9750 50  0001 C CNN
F 3 "" H 6100 9750 50  0001 C CNN
	1    6850 8500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E9EAF3F
P 8000 7400
F 0 "#PWR0101" H 8000 7150 50  0001 C CNN
F 1 "GND" V 8005 7272 50  0000 R CNN
F 2 "" H 8000 7400 50  0001 C CNN
F 3 "" H 8000 7400 50  0001 C CNN
	1    8000 7400
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 5E9EB48B
P 5550 7400
F 0 "#PWR0102" H 5550 7250 50  0001 C CNN
F 1 "+3V3" H 5565 7573 50  0000 C CNN
F 2 "" H 5550 7400 50  0001 C CNN
F 3 "" H 5550 7400 50  0001 C CNN
	1    5550 7400
	1    0    0    -1  
$EndComp
Text GLabel 7950 8450 2    39   Input ~ 0
MISO1
Text GLabel 7950 7550 2    39   Input ~ 0
MOSI1
Text GLabel 7950 8150 2    39   Input ~ 0
CS1
Text GLabel 7950 8600 2    39   Input ~ 0
SCK1
Wire Wire Line
	7850 7550 7950 7550
Wire Wire Line
	7850 8150 7950 8150
Wire Wire Line
	7850 8450 7950 8450
Wire Wire Line
	7850 8600 7950 8600
$Comp
L ADC_node:OLED1306 U5
U 1 1 5E9FDCA2
P 10400 8700
F 0 "U5" H 10978 8896 39  0000 L CNN
F 1 "OLED1306" H 10978 8821 39  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 9800 9500 39  0001 C CNN
F 3 "" H 9800 9500 39  0001 C CNN
	1    10400 8700
	1    0    0    -1  
$EndComp
Text GLabel 8150 8750 2    39   Input ~ 0
MOSI2
Text GLabel 8150 9050 2    39   Input ~ 0
CS2
Text GLabel 8150 8900 2    39   Input ~ 0
SCK2
Text GLabel 10250 7900 1    39   Input ~ 0
SCK2
Wire Wire Line
	7850 8750 8150 8750
Wire Wire Line
	7850 8900 8150 8900
Wire Wire Line
	7850 9050 8150 9050
Text GLabel 8150 9350 2    39   Input ~ 0
RESET
Text GLabel 8150 9650 2    39   Input ~ 0
DC
Wire Wire Line
	7850 9350 8150 9350
Wire Wire Line
	7850 9650 8150 9650
Text GLabel 10700 7900 1    39   Input ~ 0
DC
Text GLabel 10550 7900 1    39   Input ~ 0
RESET
Text GLabel 10850 7900 1    39   Input ~ 0
CS2
Text GLabel 10400 7900 1    39   Input ~ 0
MOSI2
$Comp
L power:+5V #PWR0103
U 1 1 5EA11DA7
P 10100 7900
F 0 "#PWR0103" H 10100 7750 50  0001 C CNN
F 1 "+5V" H 10115 8073 50  0000 C CNN
F 2 "" H 10100 7900 50  0001 C CNN
F 3 "" H 10100 7900 50  0001 C CNN
	1    10100 7900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5EA12499
P 5550 10100
F 0 "#PWR0104" H 5550 9950 50  0001 C CNN
F 1 "+5V" V 5565 10228 50  0000 L CNN
F 2 "" H 5550 10100 50  0001 C CNN
F 3 "" H 5550 10100 50  0001 C CNN
	1    5550 10100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5EA15461
P 5550 9350
F 0 "#PWR0105" H 5550 9100 50  0001 C CNN
F 1 "GND" V 5555 9222 50  0000 R CNN
F 2 "" H 5550 9350 50  0001 C CNN
F 3 "" H 5550 9350 50  0001 C CNN
	1    5550 9350
	0    1    1    0   
$EndComp
Wire Wire Line
	7850 7400 8000 7400
Text Notes -1500 3300 0    39   ~ 0
buttons, trigger, ads, reset !!!!!!!!!\n\n
$Comp
L power:GND #PWR0107
U 1 1 5EA1C100
P 9950 7900
F 0 "#PWR0107" H 9950 7650 50  0001 C CNN
F 1 "GND" H 9955 7727 50  0000 C CNN
F 2 "" H 9950 7900 50  0001 C CNN
F 3 "" H 9950 7900 50  0001 C CNN
	1    9950 7900
	-1   0    0    1   
$EndComp
Wire Wire Line
	9950 8000 9950 7900
Wire Wire Line
	10100 7900 10100 8000
Wire Wire Line
	10250 7900 10250 8000
Wire Wire Line
	10400 7900 10400 8000
Wire Wire Line
	10550 7900 10550 8000
Wire Wire Line
	10700 7900 10700 8000
Wire Wire Line
	10850 7900 10850 8000
$Comp
L power:GND #PWR0108
U 1 1 5EA3024F
P 5350 2950
F 0 "#PWR0108" H 5350 2700 50  0001 C CNN
F 1 "GND" V 5355 2822 50  0000 R CNN
F 2 "" H 5350 2950 50  0001 C CNN
F 3 "" H 5350 2950 50  0001 C CNN
	1    5350 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1700 5400 1700
Wire Wire Line
	5350 2100 5400 2100
$Comp
L ADC_node:SN760390014 U4
U 1 1 5EA32AD8
P 6850 1800
F 0 "U4" H 6875 2191 39  0000 C CNN
F 1 "SN760390014" H 6875 2116 39  0000 C CNN
F 2 "ADC_node:trafo" H 6350 2100 39  0001 C CNN
F 3 "" H 6350 2100 39  0001 C CNN
	1    6850 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5EA33DBF
P 6300 2900
F 0 "#PWR0109" H 6300 2650 50  0001 C CNN
F 1 "GND" H 6305 2727 50  0000 C CNN
F 2 "" H 6300 2900 50  0001 C CNN
F 3 "" H 6300 2900 50  0001 C CNN
	1    6300 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5EA34C19
P 6300 1450
F 0 "#PWR0110" H 6300 1300 50  0001 C CNN
F 1 "+5V" H 6315 1623 50  0000 C CNN
F 2 "" H 6300 1450 50  0001 C CNN
F 3 "" H 6300 1450 50  0001 C CNN
	1    6300 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EA36976
P 6100 2500
F 0 "C1" H 6215 2546 50  0000 L CNN
F 1 "10u" H 6215 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6138 2350 50  0001 C CNN
F 3 "~" H 6100 2500 50  0001 C CNN
	1    6100 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5EA36CE2
P 6500 2500
F 0 "C2" H 6615 2546 50  0000 L CNN
F 1 "100n" H 6615 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6538 2350 50  0001 C CNN
F 3 "~" H 6500 2500 50  0001 C CNN
	1    6500 2500
	1    0    0    -1  
$EndComp
Connection ~ 6300 1900
$Comp
L Device:D_Schottky D2
U 1 1 5EA4012F
P 7500 1700
F 0 "D2" H 7500 1484 50  0000 C CNN
F 1 "D_Schottky" H 7500 1575 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7500 1700 50  0001 C CNN
F 3 "~" H 7500 1700 50  0001 C CNN
	1    7500 1700
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Schottky D3
U 1 1 5EA408BB
P 7500 2100
F 0 "D3" H 7500 1884 50  0000 C CNN
F 1 "D_Schottky" H 7500 1975 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7500 2100 50  0001 C CNN
F 3 "~" H 7500 2100 50  0001 C CNN
	1    7500 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 5EA40C2C
P 8150 1900
F 0 "C3" H 8265 1946 50  0000 L CNN
F 1 "10u" H 8265 1855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8188 1750 50  0001 C CNN
F 3 "~" H 8150 1900 50  0001 C CNN
	1    8150 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5EA410D9
P 8550 1900
F 0 "C4" H 8665 1946 50  0000 L CNN
F 1 "100n" H 8665 1855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8588 1750 50  0001 C CNN
F 3 "~" H 8550 1900 50  0001 C CNN
	1    8550 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 1700 7850 1700
Wire Wire Line
	8550 1700 8550 1750
Wire Wire Line
	7200 1700 7350 1700
Wire Wire Line
	7650 2100 7850 2100
Wire Wire Line
	7850 2100 7850 1700
Connection ~ 7850 1700
Wire Wire Line
	7850 1700 8150 1700
Wire Wire Line
	7200 2100 7350 2100
Wire Wire Line
	7200 1900 7950 1900
Wire Wire Line
	7950 1900 7950 2100
Wire Wire Line
	7950 2100 8150 2100
Wire Wire Line
	8550 2100 8550 2050
Wire Wire Line
	8150 1750 8150 1700
Connection ~ 8150 1700
Wire Wire Line
	8150 1700 8550 1700
Wire Wire Line
	8150 2050 8150 2100
Connection ~ 8150 2100
Wire Wire Line
	8150 2100 8550 2100
Wire Wire Line
	8550 1700 8900 1700
Connection ~ 8550 1700
Wire Wire Line
	9100 2200 8900 2200
Wire Wire Line
	8900 2200 8900 1700
Connection ~ 8900 1700
Wire Wire Line
	8900 1700 9100 1700
$Comp
L power:GNDD #PWR0111
U 1 1 5EA696C9
P 9000 2900
F 0 "#PWR0111" H 9000 2650 50  0001 C CNN
F 1 "GNDD" H 9004 2745 50  0000 C CNN
F 2 "" H 9000 2900 50  0001 C CNN
F 3 "" H 9000 2900 50  0001 C CNN
	1    9000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 1950 9100 1950
$Comp
L power:+5P #PWR0112
U 1 1 5EA6AC0F
P 10500 1450
F 0 "#PWR0112" H 10500 1300 50  0001 C CNN
F 1 "+5P" H 10515 1623 50  0000 C CNN
F 2 "" H 10500 1450 50  0001 C CNN
F 3 "" H 10500 1450 50  0001 C CNN
	1    10500 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+6V #PWR0113
U 1 1 5EA6C9E9
P 8150 1550
F 0 "#PWR0113" H 8150 1400 50  0001 C CNN
F 1 "+6V" H 8165 1723 50  0000 C CNN
F 2 "" H 8150 1550 50  0001 C CNN
F 3 "" H 8150 1550 50  0001 C CNN
	1    8150 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 1700 8150 1550
$Comp
L power:GND #PWR0114
U 1 1 5EA7742E
P 6050 3950
F 0 "#PWR0114" H 6050 3700 50  0001 C CNN
F 1 "GND" V 6055 3822 50  0000 R CNN
F 2 "" H 6050 3950 50  0001 C CNN
F 3 "" H 6050 3950 50  0001 C CNN
	1    6050 3950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5EA7B27A
P 6050 4850
F 0 "#PWR0115" H 6050 4600 50  0001 C CNN
F 1 "GND" V 6055 4722 50  0000 R CNN
F 2 "" H 6050 4850 50  0001 C CNN
F 3 "" H 6050 4850 50  0001 C CNN
	1    6050 4850
	0    1    1    0   
$EndComp
Text GLabel 5400 8750 0    39   Input ~ 0
ISO_EN
Text GLabel 5100 4700 0    39   Input ~ 0
ISO_EN
Text GLabel 5100 4100 0    39   Input ~ 0
MOSI1
Text GLabel 5100 4550 0    39   Input ~ 0
MISO1
Text GLabel 5100 4250 0    39   Input ~ 0
CS1
Text GLabel 5100 4400 0    39   Input ~ 0
SCK1
$Comp
L Jumper:SolderJumper_2_Bridged JP2
U 1 1 5EA8365F
P 10900 1600
F 0 "JP2" H 10900 1700 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 10900 1714 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged_Pad1.0x1.5mm" H 10900 1600 50  0001 C CNN
F 3 "~" H 10900 1600 50  0001 C CNN
	1    10900 1600
	1    0    0    -1  
$EndComp
$Comp
L power:+5VD #PWR0116
U 1 1 5EA872F6
P 11300 1450
F 0 "#PWR0116" H 11300 1300 50  0001 C CNN
F 1 "+5VD" H 11315 1623 50  0000 C CNN
F 2 "" H 11300 1450 50  0001 C CNN
F 3 "" H 11300 1450 50  0001 C CNN
	1    11300 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 1600 10750 1600
Wire Wire Line
	11050 1600 11300 1600
Wire Wire Line
	11300 1600 11300 1450
$Comp
L Device:R R5
U 1 1 5EA8B043
P 10900 1950
F 0 "R5" V 11107 1950 50  0000 C CNN
F 1 "DNP" V 11016 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10830 1950 50  0001 C CNN
F 3 "~" H 10900 1950 50  0001 C CNN
	1    10900 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10500 1950 10750 1950
Wire Wire Line
	11050 1950 11300 1950
Wire Wire Line
	11300 1950 11300 1600
Connection ~ 11300 1600
Text Notes 10450 1200 0    39   ~ 0
digital\n+5V\n
Text Notes 11250 1200 0    39   ~ 0
analog\n+5V\n
$Comp
L Device:C C5
U 1 1 5EA97551
P 10500 2300
F 0 "C5" H 10615 2346 50  0000 L CNN
F 1 "10u" H 10615 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10538 2150 50  0001 C CNN
F 3 "~" H 10500 2300 50  0001 C CNN
	1    10500 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5EA97B5D
P 11300 2100
F 0 "C6" H 11415 2146 50  0000 L CNN
F 1 "10u" H 11415 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 11338 1950 50  0001 C CNN
F 3 "~" H 11300 2100 50  0001 C CNN
	1    11300 2100
	1    0    0    -1  
$EndComp
Connection ~ 11300 1950
$Comp
L power:GNDD #PWR0118
U 1 1 5EA98A46
P 10500 2900
F 0 "#PWR0118" H 10500 2650 50  0001 C CNN
F 1 "GNDD" H 10504 2745 50  0000 C CNN
F 2 "" H 10500 2900 50  0001 C CNN
F 3 "" H 10500 2900 50  0001 C CNN
	1    10500 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 2900 10500 2450
NoConn ~ 9850 2300
$Comp
L power:+5P #PWR0120
U 1 1 5EAB1E2F
P 7500 3800
F 0 "#PWR0120" H 7500 3650 50  0001 C CNN
F 1 "+5P" H 7515 3973 50  0000 C CNN
F 2 "" H 7500 3800 50  0001 C CNN
F 3 "" H 7500 3800 50  0001 C CNN
	1    7500 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0122
U 1 1 5EAB5F57
P 7550 3950
F 0 "#PWR0122" H 7550 3700 50  0001 C CNN
F 1 "GNDD" V 7554 3840 50  0000 R CNN
F 2 "" H 7550 3950 50  0001 C CNN
F 3 "" H 7550 3950 50  0001 C CNN
	1    7550 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDD #PWR0123
U 1 1 5EAB6573
P 7550 4850
F 0 "#PWR0123" H 7550 4600 50  0001 C CNN
F 1 "GNDD" V 7554 4740 50  0000 R CNN
F 2 "" H 7550 4850 50  0001 C CNN
F 3 "" H 7550 4850 50  0001 C CNN
	1    7550 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7400 3950 7550 3950
Wire Wire Line
	7400 3800 7500 3800
Wire Wire Line
	7400 4850 7550 4850
Wire Wire Line
	7400 4700 7500 4700
Wire Wire Line
	7500 4700 7500 3800
Connection ~ 7500 3800
Wire Wire Line
	6050 3800 6200 3800
Wire Wire Line
	6050 3950 6200 3950
Wire Wire Line
	5100 4100 5350 4100
Wire Wire Line
	5100 4250 5450 4250
Wire Wire Line
	5100 4400 5550 4400
Wire Wire Line
	5100 4550 5650 4550
Wire Wire Line
	6050 4850 6200 4850
$Comp
L power:+5VD #PWR0125
U 1 1 5EAED96C
P 14000 3950
F 0 "#PWR0125" H 14000 3800 50  0001 C CNN
F 1 "+5VD" V 14015 4078 50  0000 L CNN
F 2 "" H 14000 3950 50  0001 C CNN
F 3 "" H 14000 3950 50  0001 C CNN
	1    14000 3950
	0    1    -1   0   
$EndComp
$Comp
L power:+5P #PWR0126
U 1 1 5EAEF61D
P 11550 3800
F 0 "#PWR0126" H 11550 3650 50  0001 C CNN
F 1 "+5P" V 11565 3928 50  0000 L CNN
F 2 "" H 11550 3800 50  0001 C CNN
F 3 "" H 11550 3800 50  0001 C CNN
	1    11550 3800
	0    -1   1    0   
$EndComp
Wire Wire Line
	13100 3800 13200 3800
Wire Wire Line
	13100 4100 13200 4100
Wire Wire Line
	11550 3800 11600 3800
$Comp
L Device:C C8
U 1 1 5EB06A11
P 13400 3800
F 0 "C8" H 13285 3754 50  0000 R CNN
F 1 "1u" H 13285 3845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 13438 3650 50  0001 C CNN
F 3 "~" H 13400 3800 50  0001 C CNN
	1    13400 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	13200 3800 13200 3600
Connection ~ 13200 3600
Wire Wire Line
	13200 3600 13200 3550
Wire Wire Line
	13200 3800 13200 4100
Connection ~ 13200 3800
$Comp
L Device:C C9
U 1 1 5EB371DF
P 13750 3800
F 0 "C9" H 13635 3754 50  0000 R CNN
F 1 "100n" H 13635 3845 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 13788 3650 50  0001 C CNN
F 3 "~" H 13750 3800 50  0001 C CNN
	1    13750 3800
	-1   0    0    1   
$EndComp
Connection ~ 13750 3950
Wire Wire Line
	13750 3950 14000 3950
Wire Wire Line
	13750 3600 13750 3650
Wire Wire Line
	13100 3950 13400 3950
Wire Wire Line
	13200 3600 13400 3600
Connection ~ 13400 3950
Wire Wire Line
	13400 3950 13750 3950
Wire Wire Line
	13400 3650 13400 3600
Connection ~ 13400 3600
Wire Wire Line
	13400 3600 13750 3600
$Comp
L Device:R R6
U 1 1 5EB5B44A
P 13550 4250
F 0 "R6" V 13450 4100 50  0000 C CNN
F 1 "0R" V 13434 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 13480 4250 50  0001 C CNN
F 3 "~" H 13550 4250 50  0001 C CNN
	1    13550 4250
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5EB5BC1A
P 14500 4250
F 0 "C11" V 14400 4150 50  0000 C CNN
F 1 "4,7u" V 14400 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 14538 4100 50  0001 C CNN
F 3 "~" H 14500 4250 50  0001 C CNN
	1    14500 4250
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 5EB5F887
P 13950 4400
F 0 "C10" V 13850 4550 50  0000 C CNN
F 1 "1u" V 13850 4250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 13988 4250 50  0001 C CNN
F 3 "~" H 13950 4400 50  0001 C CNN
	1    13950 4400
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5EB62BD1
P 14000 4550
F 0 "R10" V 14100 4400 50  0000 C CNN
F 1 "0R" V 14100 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 13930 4550 50  0001 C CNN
F 3 "~" H 14000 4550 50  0001 C CNN
	1    14000 4550
	0    1    1    0   
$EndComp
$Comp
L Device:C C12
U 1 1 5EB62EFE
P 14500 4550
F 0 "C12" V 14400 4450 50  0000 C CNN
F 1 "10u" V 14400 4650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 14538 4400 50  0001 C CNN
F 3 "~" H 14500 4550 50  0001 C CNN
	1    14500 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	13100 4250 13400 4250
Wire Wire Line
	13700 4250 14350 4250
Wire Wire Line
	14650 4250 15000 4250
Wire Wire Line
	14200 4400 14200 4550
Wire Wire Line
	14200 4550 14350 4550
Wire Wire Line
	14650 4550 15000 4550
$Comp
L Device:C C7
U 1 1 5EB830D4
P 13350 5450
F 0 "C7" H 13400 5350 50  0000 C CNN
F 1 "390p" H 13200 5350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 13388 5300 50  0001 C CNN
F 3 "~" H 13350 5450 50  0001 C CNN
	1    13350 5450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R8
U 1 1 5EB842DF
P 13900 5200
F 0 "R8" V 13800 5050 50  0000 C CNN
F 1 "0R" V 13784 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 13830 5200 50  0001 C CNN
F 3 "~" H 13900 5200 50  0001 C CNN
	1    13900 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5EB84590
P 13700 5600
F 0 "R7" V 13600 5450 50  0000 C CNN
F 1 "0R" V 13584 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 13630 5600 50  0001 C CNN
F 3 "~" H 13700 5600 50  0001 C CNN
	1    13700 5600
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5EB84BD3
P 13900 5800
F 0 "R9" V 13800 5650 50  0000 C CNN
F 1 "0R" V 13784 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 13830 5800 50  0001 C CNN
F 3 "~" H 13900 5800 50  0001 C CNN
	1    13900 5800
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5E9E0F2E
P 11650 3950
F 0 "TP1" V 11650 4250 50  0000 C CNN
F 1 "TestPoint" V 11550 4100 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 11850 3950 50  0001 C CNN
F 3 "~" H 11850 3950 50  0001 C CNN
	1    11650 3950
	0    -1   -1   0   
$EndComp
Text GLabel 8450 4100 2    39   Input ~ 0
ISO_MOSI1
Text GLabel 11500 4700 0    39   Input ~ 0
ISO_MOSI1
Text GLabel 8450 4250 2    39   Input ~ 0
ISO_CS1
Text GLabel 8450 4400 2    39   Input ~ 0
ISO_SCK1
Text GLabel 8450 4550 2    39   Input ~ 0
ISO_MISO1
Text GLabel 11500 4550 0    39   Input ~ 0
ISO_CS1
Text GLabel 11500 4400 0    39   Input ~ 0
ISO_SCK1
Text GLabel 11500 4250 0    39   Input ~ 0
ISO_MISO1
$Comp
L Device:D_TVS D5
U 1 1 5EA06247
P 14550 5400
F 0 "D5" V 14504 5479 50  0000 L CNN
F 1 "PGSMAJ10CA E3G" H 14250 5650 50  0000 L CNN
F 2 "Diode_SMD:D_3220_8050Metric_Castellated" H 14550 5400 50  0001 C CNN
F 3 "~" H 14550 5400 50  0001 C CNN
	1    14550 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	13100 4700 13250 4700
Wire Wire Line
	13250 4700 13250 5200
Wire Wire Line
	13250 5200 13350 5200
Wire Wire Line
	13350 5300 13350 5200
Connection ~ 13350 5200
Wire Wire Line
	13350 5200 13750 5200
Wire Wire Line
	13100 4850 13150 4850
Wire Wire Line
	13150 4850 13150 5600
Wire Wire Line
	13150 5600 13350 5600
Wire Wire Line
	13350 5600 13550 5600
Connection ~ 13350 5600
Wire Wire Line
	13350 5600 13350 5800
Wire Wire Line
	13350 5800 13750 5800
Wire Wire Line
	14050 5200 14550 5200
Wire Wire Line
	14550 5200 14550 5250
Wire Wire Line
	14050 5800 14400 5800
Wire Wire Line
	14550 5800 14550 5850
Wire Wire Line
	14400 5600 14400 5800
Connection ~ 14400 5800
Wire Wire Line
	14400 5800 14550 5800
Wire Wire Line
	14550 5550 14550 5800
Connection ~ 14550 5800
$Comp
L Device:R R11
U 1 1 5EA3590C
P 14100 5600
F 0 "R11" V 14000 5450 50  0000 C CNN
F 1 "0R" V 13984 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 14030 5600 50  0001 C CNN
F 3 "~" H 14100 5600 50  0001 C CNN
	1    14100 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	13850 5600 13950 5600
Wire Wire Line
	14250 5600 14400 5600
Wire Wire Line
	13100 4400 13800 4400
Wire Wire Line
	14150 4550 14200 4550
Connection ~ 14200 4550
Wire Wire Line
	14100 4400 14200 4400
Wire Wire Line
	11500 4700 11700 4700
Wire Wire Line
	11500 4550 11700 4550
Wire Wire Line
	11500 4400 11700 4400
Wire Wire Line
	11500 4250 11700 4250
$Comp
L Connector:TestPoint TP2
U 1 1 5EAAD96F
P 11650 4100
F 0 "TP2" V 11650 4400 50  0000 C CNN
F 1 "TestPoint" V 11550 4250 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 11850 4100 50  0001 C CNN
F 3 "~" H 11850 4100 50  0001 C CNN
	1    11650 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11650 3950 11700 3950
Wire Wire Line
	11650 4100 11700 4100
Wire Wire Line
	5800 5250 5650 5250
Wire Wire Line
	5800 5350 5550 5350
Wire Wire Line
	5450 5450 5800 5450
Wire Wire Line
	5800 5550 5350 5550
$Comp
L Connector:Conn_01x05_Male J2
U 1 1 5EAF013C
P 6000 5350
F 0 "J2" H 5972 5282 50  0000 R CNN
F 1 "Conn_01x05_Male" H 5972 5373 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6000 5350 50  0001 C CNN
F 3 "~" H 6000 5350 50  0001 C CNN
	1    6000 5350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_Coaxial J3
U 1 1 5EB05B88
P 15100 5200
F 0 "J3" H 15200 5175 50  0000 L CNN
F 1 "Conn_Coaxial" H 15200 5084 50  0000 L CNN
F 2 "ADC_node:SMB_Jack_Vertical" H 15100 5200 50  0001 C CNN
F 3 " ~" H 15100 5200 50  0001 C CNN
	1    15100 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	15100 5850 15100 5400
Wire Wire Line
	13100 4550 13850 4550
$Comp
L Device:LED D4
U 1 1 5EB74B35
P 10150 1900
F 0 "D4" V 10189 1783 50  0000 R CNN
F 1 "LED" V 10098 1783 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric" H 10150 1900 50  0001 C CNN
F 3 "~" H 10150 1900 50  0001 C CNN
	1    10150 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5EB754CF
P 10150 2300
F 0 "R4" V 10357 2300 50  0000 C CNN
F 1 "1k" V 10266 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10080 2300 50  0001 C CNN
F 3 "~" H 10150 2300 50  0001 C CNN
	1    10150 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0132
U 1 1 5EB7870D
P 10150 2900
F 0 "#PWR0132" H 10150 2650 50  0001 C CNN
F 1 "GNDD" H 10200 2750 50  0000 R CNN
F 2 "" H 10150 2900 50  0001 C CNN
F 3 "" H 10150 2900 50  0001 C CNN
	1    10150 2900
	1    0    0    -1  
$EndComp
Connection ~ 10500 1600
Connection ~ 10500 1950
Wire Wire Line
	10500 1950 10500 2150
Wire Wire Line
	10500 1600 10500 1950
Wire Wire Line
	10500 1450 10500 1600
Wire Wire Line
	9850 1600 10150 1600
Wire Wire Line
	10150 1600 10150 1750
Connection ~ 10150 1600
Wire Wire Line
	10150 1600 10500 1600
Wire Wire Line
	10150 2050 10150 2150
Wire Wire Line
	10150 2450 10150 2900
Wire Wire Line
	11300 2250 11300 2900
Wire Wire Line
	6100 2350 6300 2350
Wire Wire Line
	6100 2650 6300 2650
Wire Wire Line
	6300 1900 6300 1450
Wire Wire Line
	6300 1900 6550 1900
Wire Wire Line
	6300 2350 6300 1900
Connection ~ 6300 2350
Wire Wire Line
	6300 2350 6500 2350
Wire Wire Line
	6300 2650 6300 2900
Connection ~ 6300 2650
Wire Wire Line
	6300 2650 6500 2650
Wire Wire Line
	5350 2100 5350 2950
Connection ~ 5350 2100
Wire Wire Line
	5350 1700 5350 2100
Wire Wire Line
	14550 5200 14900 5200
Connection ~ 14550 5200
$Comp
L Device:Rotary_Encoder_Switch SW1
U 1 1 5ED1E524
P 2200 8650
F 0 "SW1" H 2400 8900 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2200 8926 50  0001 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 2050 8810 50  0001 C CNN
F 3 "~" H 2200 8910 50  0001 C CNN
	1    2200 8650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 5ED284F7
P 5200 8600
F 0 "J1" H 5250 8500 50  0000 C CNN
F 1 "Conn_01x01_Female" H 5092 8466 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5200 8600 50  0001 C CNN
F 3 "~" H 5200 8600 50  0001 C CNN
	1    5200 8600
	-1   0    0    1   
$EndComp
Text Notes 4550 8450 0    50   ~ 0
external trigger
$Comp
L power:GND #PWR0133
U 1 1 5ED362F4
P 1400 8650
F 0 "#PWR0133" H 1400 8400 50  0001 C CNN
F 1 "GND" V 1405 8522 50  0000 R CNN
F 2 "" H 1400 8650 50  0001 C CNN
F 3 "" H 1400 8650 50  0001 C CNN
	1    1400 8650
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5ED39EB6
P 1600 8150
F 0 "R1" V 1500 8000 50  0000 C CNN
F 1 "10k" V 1716 8150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 8150 50  0001 C CNN
F 3 "~" H 1600 8150 50  0001 C CNN
	1    1600 8150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5ED3D1E4
P 1800 8150
F 0 "R2" V 1900 8000 50  0000 C CNN
F 1 "10k" V 1916 8150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1730 8150 50  0001 C CNN
F 3 "~" H 1800 8150 50  0001 C CNN
	1    1800 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 8650 1900 8650
Wire Wire Line
	1600 8300 1600 8750
Wire Wire Line
	1600 8750 1900 8750
Wire Wire Line
	1800 8300 1800 8550
Wire Wire Line
	1800 8550 1900 8550
$Comp
L power:GND #PWR0134
U 1 1 5ED73B5D
P 2500 8550
F 0 "#PWR0134" H 2500 8300 50  0001 C CNN
F 1 "GND" V 2505 8422 50  0000 R CNN
F 2 "" H 2500 8550 50  0001 C CNN
F 3 "" H 2500 8550 50  0001 C CNN
	1    2500 8550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 8750 2800 8750
Wire Wire Line
	1800 8550 1800 8900
Connection ~ 1800 8550
Wire Wire Line
	1600 9050 1600 8750
Connection ~ 1600 8750
Wire Wire Line
	1600 8000 1600 7950
Wire Wire Line
	1800 7950 1800 8000
Connection ~ 1800 7950
$Comp
L Device:R R3
U 1 1 5EDA767C
P 2000 8150
F 0 "R3" V 2100 8000 50  0000 C CNN
F 1 "10k" V 2116 8150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1930 8150 50  0001 C CNN
F 3 "~" H 2000 8150 50  0001 C CNN
	1    2000 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 8300 2000 8350
Wire Wire Line
	2000 8350 2800 8350
Wire Wire Line
	2800 8350 2800 8750
Connection ~ 2800 8750
Wire Wire Line
	1800 7950 2000 7950
Wire Wire Line
	2000 7950 2000 8000
Wire Wire Line
	1800 7850 1800 7950
$Comp
L LED:WS2812B D1
U 1 1 5EDCC0F0
P 1850 10100
F 0 "D1" H 1506 10146 50  0000 R CNN
F 1 "WS2812B" H 1506 10055 50  0000 R CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 1900 9800 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 1950 9725 50  0001 L TNN
	1    1850 10100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 7950 1800 7950
NoConn ~ 1550 10100
$Comp
L power:GND #PWR0137
U 1 1 5EE15A09
P 1850 10450
F 0 "#PWR0137" H 1850 10200 50  0001 C CNN
F 1 "GND" V 1855 10322 50  0000 R CNN
F 2 "" H 1850 10450 50  0001 C CNN
F 3 "" H 1850 10450 50  0001 C CNN
	1    1850 10450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 9800 1850 9750
Wire Wire Line
	1850 10400 1850 10450
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5EE4B13B
P 1700 6000
F 0 "H1" H 1800 6003 50  0000 L CNN
F 1 "MountingHole_Pad" H 1800 5958 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1700 6000 50  0001 C CNN
F 3 "~" H 1700 6000 50  0001 C CNN
	1    1700 6000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5EE4D86C
P 2150 6000
F 0 "H3" H 2250 6003 50  0000 L CNN
F 1 "MountingHole_Pad" H 2250 5958 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2150 6000 50  0001 C CNN
F 3 "~" H 2150 6000 50  0001 C CNN
	1    2150 6000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H5
U 1 1 5EE4DAD5
P 2600 6000
F 0 "H5" H 2700 6003 50  0000 L CNN
F 1 "MountingHole_Pad" H 2700 5958 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2600 6000 50  0001 C CNN
F 3 "~" H 2600 6000 50  0001 C CNN
	1    2600 6000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5EE4E4B9
P 1700 6350
F 0 "H2" H 1800 6353 50  0000 L CNN
F 1 "MountingHole_Pad" H 1800 6308 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 1700 6350 50  0001 C CNN
F 3 "~" H 1700 6350 50  0001 C CNN
	1    1700 6350
	1    0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5EE4E4C3
P 2150 6350
F 0 "H4" H 2250 6353 50  0000 L CNN
F 1 "MountingHole_Pad" H 2250 6308 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2150 6350 50  0001 C CNN
F 3 "~" H 2150 6350 50  0001 C CNN
	1    2150 6350
	1    0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H6
U 1 1 5EE4E4CD
P 2600 6350
F 0 "H6" H 2700 6353 50  0000 L CNN
F 1 "MountingHole_Pad" H 2700 6308 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2600 6350 50  0001 C CNN
F 3 "~" H 2600 6350 50  0001 C CNN
	1    2600 6350
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0138
U 1 1 5EE68C4D
P 2950 6150
F 0 "#PWR0138" H 2950 5900 50  0001 C CNN
F 1 "GND" V 2955 6022 50  0000 R CNN
F 2 "" H 2950 6150 50  0001 C CNN
F 3 "" H 2950 6150 50  0001 C CNN
	1    2950 6150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 1700 6550 1700
Wire Wire Line
	6300 1900 6100 1900
Wire Wire Line
	6100 2100 6550 2100
Wire Wire Line
	1700 6100 1700 6150
Wire Wire Line
	1700 6150 1700 6250
Connection ~ 1700 6150
Wire Wire Line
	2150 6250 2150 6150
Wire Wire Line
	2150 6150 2150 6100
Wire Wire Line
	2150 6150 1700 6150
Connection ~ 2150 6150
Wire Wire Line
	2600 6100 2600 6150
Wire Wire Line
	2600 6150 2600 6250
Wire Wire Line
	2950 6150 2600 6150
Wire Wire Line
	2600 6150 2150 6150
Connection ~ 2600 6150
$Comp
L ADC_node:SN6501DBVT U2
U 1 1 5EEFF634
P 5750 1700
F 0 "U2" H 5750 1925 50  0000 C CNN
F 1 "SN6501DBVT" H 5750 1834 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5750 1700 50  0001 C CNN
F 3 "" H 5750 1700 50  0001 C CNN
	1    5750 1700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 5150 5750 5150
Wire Wire Line
	5750 5150 5750 5650
Wire Wire Line
	5750 5650 5600 5650
$Comp
L power:GNDD #PWR0139
U 1 1 5EFB8CB7
P 8150 2900
F 0 "#PWR0139" H 8150 2650 50  0001 C CNN
F 1 "GNDD" H 8154 2745 50  0000 C CNN
F 2 "" H 8150 2900 50  0001 C CNN
F 3 "" H 8150 2900 50  0001 C CNN
	1    8150 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2100 8150 2900
$Comp
L Device:C C13
U 1 1 5EA9BDE8
P 11600 3550
F 0 "C13" H 11485 3504 50  0000 R CNN
F 1 "1u" H 11485 3595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 11638 3400 50  0001 C CNN
F 3 "~" H 11600 3550 50  0001 C CNN
	1    11600 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 1950 9000 2900
$Comp
L power:GNDD #PWR0117
U 1 1 5EAAB31A
P 11300 2900
F 0 "#PWR0117" H 11300 2650 50  0001 C CNN
F 1 "GNDD" H 11304 2745 50  0000 C CNN
F 2 "" H 11300 2900 50  0001 C CNN
F 3 "" H 11300 2900 50  0001 C CNN
	1    11300 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0119
U 1 1 5EAAFA79
P 13200 3550
F 0 "#PWR0119" H 13200 3300 50  0001 C CNN
F 1 "GNDD" H 13204 3395 50  0000 C CNN
F 2 "" H 13200 3550 50  0001 C CNN
F 3 "" H 13200 3550 50  0001 C CNN
	1    13200 3550
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR0124
U 1 1 5EAB30CC
P 15000 4250
F 0 "#PWR0124" H 15000 4000 50  0001 C CNN
F 1 "GNDD" H 15004 4095 50  0000 C CNN
F 2 "" H 15000 4250 50  0001 C CNN
F 3 "" H 15000 4250 50  0001 C CNN
	1    15000 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDD #PWR0127
U 1 1 5EAB344A
P 15000 4550
F 0 "#PWR0127" H 15000 4300 50  0001 C CNN
F 1 "GNDD" H 15004 4395 50  0000 C CNN
F 2 "" H 15000 4550 50  0001 C CNN
F 3 "" H 15000 4550 50  0001 C CNN
	1    15000 4550
	0    -1   -1   0   
$EndComp
$Comp
L power:GNDD #PWR0128
U 1 1 5EAB3760
P 14550 5850
F 0 "#PWR0128" H 14550 5600 50  0001 C CNN
F 1 "GNDD" H 14554 5695 50  0000 C CNN
F 2 "" H 14550 5850 50  0001 C CNN
F 3 "" H 14550 5850 50  0001 C CNN
	1    14550 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0129
U 1 1 5EAB3A9C
P 15100 5850
F 0 "#PWR0129" H 15100 5600 50  0001 C CNN
F 1 "GNDD" H 15104 5695 50  0000 C CNN
F 2 "" H 15100 5850 50  0001 C CNN
F 3 "" H 15100 5850 50  0001 C CNN
	1    15100 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0131
U 1 1 5EAD2E3A
P 11600 3350
F 0 "#PWR0131" H 11600 3100 50  0001 C CNN
F 1 "GNDD" H 11604 3195 50  0000 C CNN
F 2 "" H 11600 3350 50  0001 C CNN
F 3 "" H 11600 3350 50  0001 C CNN
	1    11600 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	11600 3800 11600 3700
Connection ~ 11600 3800
Wire Wire Line
	11600 3800 11700 3800
Wire Wire Line
	11600 3400 11600 3350
Wire Wire Line
	7400 4100 8450 4100
Wire Wire Line
	7400 4250 8450 4250
Wire Wire Line
	7400 4400 8450 4400
Wire Wire Line
	7400 4550 8450 4550
Wire Wire Line
	5350 4100 5350 5550
Connection ~ 5350 4100
Wire Wire Line
	5350 4100 6200 4100
Wire Wire Line
	5450 4250 5450 5450
Connection ~ 5450 4250
Wire Wire Line
	5450 4250 6200 4250
Wire Wire Line
	5550 4400 5550 5350
Connection ~ 5550 4400
Wire Wire Line
	5550 4400 6200 4400
Wire Wire Line
	5650 4550 5650 5250
Connection ~ 5650 4550
Wire Wire Line
	5650 4550 6200 4550
$Comp
L Connector:TestPoint TP3
U 1 1 5EB9F9B3
P 13000 7350
F 0 "TP3" V 13000 7650 50  0000 C CNN
F 1 "TestPoint" V 12900 7500 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 13200 7350 50  0001 C CNN
F 3 "~" H 13200 7350 50  0001 C CNN
	1    13000 7350
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0140
U 1 1 5EBA1764
P 13000 7600
F 0 "#PWR0140" H 13000 7350 50  0001 C CNN
F 1 "GNDD" H 13004 7445 50  0000 C CNN
F 2 "" H 13000 7600 50  0001 C CNN
F 3 "" H 13000 7600 50  0001 C CNN
	1    13000 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	13000 7600 13000 7350
$Comp
L power:GND #PWR0130
U 1 1 5EBBCE2C
P 5600 5650
F 0 "#PWR0130" H 5600 5400 50  0001 C CNN
F 1 "GND" V 5605 5522 50  0000 R CNN
F 2 "" H 5600 5650 50  0001 C CNN
F 3 "" H 5600 5650 50  0001 C CNN
	1    5600 5650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 5EC392BA
P 5200 8450
F 0 "J4" H 5250 8350 50  0000 C CNN
F 1 "Conn_01x01_Female" H 5092 8316 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5200 8450 50  0001 C CNN
F 3 "~" H 5200 8450 50  0001 C CNN
	1    5200 8450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5100 4700 6200 4700
NoConn ~ 5800 7550
NoConn ~ 5800 7700
NoConn ~ 5800 7850
NoConn ~ 5800 8900
NoConn ~ 5800 9650
NoConn ~ 5800 9800
NoConn ~ 5800 9950
NoConn ~ 7850 9800
NoConn ~ 7850 9950
NoConn ~ 7850 10100
NoConn ~ 7850 9200
NoConn ~ 7850 8300
NoConn ~ 7850 8000
NoConn ~ 7850 7850
NoConn ~ 7850 7700
Text GLabel 2900 8900 2    39   Input ~ 0
ENC1
Text GLabel 2900 9050 2    39   Input ~ 0
ENC2
Text GLabel 2900 8750 2    39   Input ~ 0
SW
Wire Wire Line
	2800 8750 2900 8750
Wire Wire Line
	1800 8900 2900 8900
Wire Wire Line
	1600 9050 2900 9050
Text GLabel 5700 9200 0    39   Input ~ 0
ENC1
Text GLabel 5700 9500 0    39   Input ~ 0
ENC2
Wire Wire Line
	5700 9200 5800 9200
Wire Wire Line
	5550 9350 5800 9350
Wire Wire Line
	5400 8600 5800 8600
Wire Wire Line
	5400 8450 5800 8450
Wire Wire Line
	5400 8750 5800 8750
Wire Wire Line
	5700 9500 5800 9500
Text GLabel 8150 9500 2    39   Input ~ 0
SW
Text GLabel 2550 10100 2    39   Input ~ 0
RGB_LED
Text GLabel 5700 9050 0    39   Input ~ 0
RGB_LED
Wire Wire Line
	2150 10100 2550 10100
Wire Wire Line
	5700 9050 5800 9050
Wire Wire Line
	5550 10100 5800 10100
$Comp
L power:+3V3 #PWR0106
U 1 1 5EC61F07
P 1850 9750
F 0 "#PWR0106" H 1850 9600 50  0001 C CNN
F 1 "+3V3" H 1865 9923 50  0000 C CNN
F 2 "" H 1850 9750 50  0001 C CNN
F 3 "" H 1850 9750 50  0001 C CNN
	1    1850 9750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 7400 5800 7400
$Comp
L power:+3V3 #PWR0121
U 1 1 5EC8C86F
P 6050 3800
F 0 "#PWR0121" H 6050 3650 50  0001 C CNN
F 1 "+3V3" H 6065 3973 50  0000 C CNN
F 2 "" H 6050 3800 50  0001 C CNN
F 3 "" H 6050 3800 50  0001 C CNN
	1    6050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 9500 8150 9500
$Comp
L power:+3V3 #PWR0135
U 1 1 5ECB04DB
P 1800 7850
F 0 "#PWR0135" H 1800 7700 50  0001 C CNN
F 1 "+3V3" H 1815 8023 50  0000 C CNN
F 2 "" H 1800 7850 50  0001 C CNN
F 3 "" H 1800 7850 50  0001 C CNN
	1    1800 7850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
