
go: 
	ampy -p /dev/ttyUSB0 -d 1 put sources/boot.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/main.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/ssd1306.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/robust.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/encdec.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/wifimgr.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/rotary.py
	ampy -p /dev/ttyUSB0 -d 1 put sources/rotary_irq_esp.py
boot:
	ampy -p /dev/ttyUSB0 put sources/boot.py
reflash: 
	esptool.py -p /dev/ttyUSB0 erase_flash
	esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 921600 \
	write_flash -z 0x1000 esp32-idf3-20200114-v1.12-63-g1c849d63a.bin
reflash8266: 
	esptool.py -p /dev/ttyUSB0 erase_flash
	esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20191220-v1.12.bin
erase: 
	ampy -p /dev/ttyUSB0 rm main.py
	ampy -p /dev/ttyUSB0 rm boot.py
ls: 
	ampy -p /dev/ttyUSB0 ls



#esptool.py --chip esp32 --port /dev/ttyUSB1 --baud 921600 write_flash -z 0x1000 esp32-idf3-20200114-v1.12-63-g1c849d63a.bin 
#esptool.py -p /dev/ttyUSB1 --baud 921600 erase_flash

