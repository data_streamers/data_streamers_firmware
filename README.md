
![rmxhardware](pictures/adcnode_setup.jpeg)
![rmxhardware](pictures/adcnode_running.jpeg)
# ADC node function:

On first powering up, device creates a wifi hotspot named ADCNODE, publish a login website at 192.168.4.1, password is setup123.
Here you can fill credentials of your local wifi. Once set, device will store it in the non volatile memory.
After reboot, it will connect automatically. 

Once you have device connected, you just set A/D conversion range and frequency. Device then starts to stream data into local database. You can walk through data by using your browser to view grafana instance on localhost:3000.



# ADC node firmware
(Debian 10)
### Dependencies:

Install Python3, then with pip3:

```sudo pip3 install adafruit-ampy```

```pip3 install esptool```

### Rules 
Create file with rules:

```touch /etc/udev/rules.d/99-esp32.rules ```

Add this line in:

```ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", MODE="664", GROUP="plugdev"```


reload rules:

```sudo udevadm control --reload-rules && sudo udevadm trigger```

### Preparing the board (we use esp32):
By "preparing" I mean flashing it with micropython binary.

A) esp32:

   ```make erase```

   ```make reflash```

B) esp8266

   ```make erase```

   ```make reflash8266```

The board should be uploaded with micropython and ready to work. You can check it by connecting to serialport, there should be a micropython console visible on this device.


# FW update, flashing: 

Do this after changing the code, then reset the board with "reset" button from the bottom.

```make go```

Note: by calling "make go", makefile will try to upload all python files. For most of the changes, only uploading make.py should be sufficient, you can do it by calling 
``` ampy -p /dev/ttyUSB0 -d 1 put sources/main.py ```.

Just have a look into the Makefile,)

Note n.2: For succesfull upload, it is recommended to flash boot.py the first. Once all files are there, you can flash the single one with an example above.

# Development recommendations:

If you areconnected and listening on the device's serialport, ampy is not able to flash python files. Therefore I recommend:

1) Change the code
2) Upload it by calling make go
3) Open serial port
4) Press reset button and observe booting sequence and errors.

-- modify and repeat

