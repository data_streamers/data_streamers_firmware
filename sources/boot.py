'''
from machine import Pin, I2C, SPI
import machine
import ssd1306

def connect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect('Rooftop', 'NemamNapadyVarimVyvar')
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())
'''
def no_debug():
    import esp
    # you can run this from the REPL as well
    esp.osdebug(None)

'''
spi2 = SPI(-1, baudrate=500000, polarity=0, phase=0, sck=Pin(17), mosi=Pin(5), miso=Pin(4))
oled = ssd1306.SSD1306_SPI(128, 64, spi2, machine.Pin(15), machine.Pin(0), machine.Pin(16)) #data res cs
oled.fill(0)
oled.text('Connecting', 15, 0)
oled.text("wait..", 32, 56)
oled.show()
'''

no_debug()
#connect()

