
from machine import Pin, I2C, SPI
import machine
import time
from time import sleep
import wifimgr

from umqtt.robust import MQTTClient
import ssd1306
import network
import machine, neopixel

try:
  import usocket as socket
except:
  import socket

from rotary_irq_esp import RotaryIRQ


r = RotaryIRQ(pin_num_clk=12, pin_num_dt=13, min_val=0, max_val=8, reverse=False, range_mode=RotaryIRQ.RANGE_WRAP)
switch = Pin(2, Pin.IN, Pin.PULL_UP)

# display
spi2 = SPI(-1, baudrate=500000, polarity=0, phase=0, sck=Pin(17), mosi=Pin(5), miso=Pin(4))
oled = ssd1306.SSD1306_SPI(128, 64, spi2, machine.Pin(15), machine.Pin(0), machine.Pin(16)) #data res cs

# ADC IC
spi1 = machine.SPI(2, baudrate=8000000, polarity=0, phase=0, bits=8, firstbit=0, sck=machine.Pin(18), mosi=machine.Pin(23), miso=machine.Pin(19))
CS = machine.Pin(21, machine.Pin.OUT, machine.Pin.PULL_UP)

# neopixel LED
np = neopixel.NeoPixel(machine.Pin(14), 1)

# intro screen
oled.fill(0)
oled.text("Connecting", 21, 7)
oled.show()


wlan = wifimgr.get_connection()

if wlan is None:
    print("Could not initialize the network connection.")
    while True:
        pass  # you shall not pass :D


# MQTT setup
# Each node must have own ID, you can create one for example with this call:
# python3 -c 'from uuid import uuid4; print(uuid4())'
#
# disposable pregenerated IDs:
#2faaca32-c358-406b-ae99-8aae3dd7eed5
#d3d22183-d9db-43c0-b5fa-af8d5cb74a67
#5fa5c8be-7948-47a3-942d-b986196e9afc
#fdd11a3f-c693-4d4d-b85b-a338b7cea0b2
client = MQTTClient('ce6ea14f-8f4d-4806-9950-751dc848e4f0', '192.168.50.116')

client.connect()

# network ip address
sta_if = network.WLAN(network.STA_IF)
IP = sta_if.ifconfig()[0]

# Setup the range of ADC chip
def setup_range():
    np[0] = (42, 0, 0)
    np.write()

    val_old = r.value()
    while(True):
        val_new = r.value()
        if val_old != val_new:
            val_old = val_new

        if(val_new == 0):
            byte_to_send = 0b0000
            Range_str = '+-12.288V'
        if(val_new == 1):
            byte_to_send = 0b0001
            Range_str = '+-10.24V'
        if(val_new == 2):
            byte_to_send = 0b0010
            Range_str = '+-6.144V'
        if(val_new == 3):
            byte_to_send = 0b0011
            Range_str = '+-5.12V'
        if(val_new == 4):
            byte_to_send = 0b0100
            Range_str = '+-2.56V'
        if(val_new == 5):
            byte_to_send = 0b1000
            Range_str = '12.288V'
        if(val_new == 6):
            byte_to_send = 0b1001
            Range_str = '10.24V'
        if(val_new == 7):
            byte_to_send = 0b1010
            Range_str = '6.144V'
        if(val_new == 8):
            byte_to_send = 0b1011
            Range_str = '5.12V'

        oled.fill(0)
        oled.text('SELECT RANGE', 15, 0)
        oled.text(Range_str, 30, 28)
        oled.text("PRESS OK", 32, 56)
        oled.show()
      
        if(button_pressed()):
            CS.value(0)
            cmd = bytearray((0xD0, 0x14, 0x00, byte_to_send))#11010000 00010100 
            res = bytearray(4)
            spi1.write_readinto(cmd, res)
            CS.value(1) 

            np[0] = (0, 42, 0)
            np.write()
 
            return Range_str, val_new
  
# Setup the timeout between single measures
def setup_freq():
    np[0] = (42, 0, 0)
    np.write()

    val_old = r.value()
    while(True):
        val_new = r.value()
        if val_old != val_new:
            val_old = val_new

        if(val_new == 0):
            Freq_str = '0.1Hz'
            Freq_num = 10000000
        if(val_new == 1):
            Freq_str = '0.5Hz'
            Freq_num = 2000000
        if(val_new == 2):
            Freq_str = '1Hz'
            Freq_num = 1000000
        if(val_new == 3):
            Freq_str = '2Hz'
            Freq_num = 500000
        if(val_new == 4):
            Freq_str = '5Hz'
            Freq_num = 200000
        if(val_new == 5):
            Freq_str = '10Hz'
            Freq_num = 100000
        if(val_new == 6):
            Freq_str = '100Hz'
            Freq_num = 10000
        if(val_new == 7):
            Freq_str = '1000Hz'
            Freq_num = 1000
        if(val_new == 8):
            Freq_str = '10000Hz'
            Freq_num = 100

        oled.fill(0)
        oled.text('SELECT FREQ', 15, 0)
        oled.text(Freq_str, 30, 28)
        oled.text("PRESS OK", 32, 56)
        oled.show()
      
        if(button_pressed()):
            np[0] = (0, 42, 0)
            np.write()
            return Freq_str, Freq_num

# Read data from ADC node
def read_data (Range_num):
    CS.value(0)
    cmd = bytearray((0xff, 0xff, 0xff, 0xff))#11010000 00010100 
    res= bytearray(4)
    spi1.write_readinto(cmd, res)
    CS.value(1)

    result = (res[0] << 8) + res[1]

    if(Range_num == 0):
        result = ((result * 4.096 * 6.0) / 0xffff) - (3.0 * 4.096)
    if(Range_num == 1):
        result = ((result * 4.096 * 5.0) / 0xffff) - (2.5 * 4.096)
    if(Range_num == 2):
        result = ((result * 4.096 * 3.0) / 0xffff) - (1.5 * 4.096)
    if(Range_num == 3):
        result = ((result * 4.096 * 2.5) / 0xffff) - (1.25 * 4.096)
    if(Range_num == 4):
        result = ((result * 4.096 * 1.25) / 0xffff) - (0.625 * 4.096)
    if(Range_num == 5):
        result = ((result * 4.096 * 3.0) / 0xffff) 
    if(Range_num == 6):
        result = ((result * 4.096 * 2.5) / 0xffff) 
    if(Range_num == 7):
        result = ((result * 4.096 * 1.5) / 0xffff) 
    if(Range_num == 8):
        result = ((result * 4.096 * 1.25) / 0xffff) 
    return result

# Checks if button is pressed
def button_pressed():
    if(not(switch.value())):
        while(not(switch.value())):
            pass
        return 1
    return 0

# Display values on OLED
def display(mode, frequency, ip, value):
    oled.fill(0)
    oled.text(Range_str, 0, 0)
    oled.text(str(frequency), 73, 0)
    oled.text(ip, 0, 14)
    oled.text(value + ' V', 32, 40)
    oled.show()

# Menu setup
def menu():
    val_old = r.value()
    while(True):
        val_new = r.value()
        if val_old != val_new:
            val_old = val_new
        oled.fill(0)
        if(val_old % 2):
            oled.text(">", 10, 21)
        else:
            oled.text(">", 10, 32)
        oled.text("SETUP", 42, 21)
        oled.text("START", 42, 32)
        oled.show()
        if(button_pressed()):
            if(val_old % 2):
                Range_str = setup_range()
            else:
                break

# Init values
Freq_str =  "0.1Hz"
Freq_num = 0
Range_str = '+-12V'
Range_num = 0


# If everything goes well, device should be connected and is ready for setup:
oled.fill(0)
oled.text("Device", 21, 21)
oled.text("connected!", 17, 35)
oled.show()
time.sleep(1)

Range_str, Range_num = setup_range()
Freq_str, Freq_num = setup_freq()


readed = 0

while True:
    result  = read_data(Range_num)
    result = '%.6f' % result
    if readed == 0:
        display(Range_str, Freq_str, IP, result)
    client.publish('datastreamers/voltage/adc', str(result))
    if(button_pressed()):
        Range_str, Range_num = setup_range()
        Freq_str, Freq_num = setup_freq()
    time.sleep_us(Freq_num)

    if readed >50
        readed = 0
    else:
      readed = readed+1



        

